import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@TestInstance(value = TestInstance.Lifecycle.PER_CLASS) // to create the same test obj
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    public void setup() {
        calculator = new Calculator();
    }

    @Test
    @DisplayName("Adding test")
    @Order(1)
    public void addTest() {
        int res = calculator.add(1, 2);
        assertEquals(3, res);
    }

    @Test
    @DisplayName("Subtraction test")
    @Order(3)
    public void subTest() {
        int res = calculator.sub(1, 2);
        assertEquals(-1, res);
    }

    @Test
    @DisplayName("Multiply test")
    @Order(2)
    public void multiplyTest() {
        int res = calculator.multiply(3, 2);
        assertEquals(6, res);
    }

    @Test
    @DisplayName("Divide test")
    @Order(4)
    public void divTest() {
        int res = calculator.div(4, 2);
        assertEquals(2, res);
    }

    @Test
    @DisplayName("Exception thrown test")
    @Order(5)
    public void divByZeroTest() {
        assertThrows(ArithmeticException.class, () -> calculator.div(4, 0));
    }
}

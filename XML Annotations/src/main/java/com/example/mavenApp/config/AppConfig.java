package com.example.mavenApp.config;

import com.example.mavenApp.model.Course;
import com.example.mavenApp.model.Student;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("name")
    private String name;

    @Bean
    public Course getCourse() {
        return new Course(name, 12);
    }

    @Bean
    public Student getStudent() {
        return new Student("Ana", 25, getCourse());
    }
}

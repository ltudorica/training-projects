package com.example.mavenApp;

import com.example.mavenApp.config.AppConfig;
import com.example.mavenApp.model.Course;
import com.example.mavenApp.model.Student;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
        context.registerShutdownHook();

        Student student = (Student) context.getBean("student");
        System.out.println(student.printDetails());

        System.out.println(student);
        System.out.println(student.getCourse());
        System.out.println(student.getCourse());

        Course course = (Course) context.getBean("course");
        System.out.println(course);
        Course course1 = (Course) context.getBean("course");
        System.out.println(course1);

        System.out.println();
        System.out.println("-----------------------------------");

        Student student1 = (Student) context.getBean("student1");
        System.out.println(student1.getCourse());

        context.close();

        System.out.println();
        System.out.println("-----------------------------------");

        AnnotationConfigApplicationContext context2 = null;

        try {
            context2 = new AnnotationConfigApplicationContext(AppConfig.class);
            Course newCourse = context2.getBean("getCourse", Course.class);
            System.out.println(newCourse.printDetails());

            Student student3 = context2.getBean("getStudent", Student.class);
            System.out.println(student3.printDetails());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (context2 != null) {
                context2.close();
            }
        }
    }
}

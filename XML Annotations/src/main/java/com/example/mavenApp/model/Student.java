package com.example.mavenApp.model;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Student implements InitializingBean, DisposableBean {
    private String name;
    private int age;
    private Course course;

    public Student() {
        System.out.println("Student default constructor");
    }

    public Student(String name, int age, Course course) {
        this.name = name;
        this.age = age;
        this.course = course;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public void customInitMethod() {
        System.out.println("This is the custom init method");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("After Properties method\r\n");
    }

    public String printDetails() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", course=" + course.getName() +
                '}';
    }


    @Override
    public void destroy() throws Exception {
        System.out.println("DEFAULT Destroy method");
    }

    private void customDestroyMethod() {
        System.out.println("CUSTOM Destroy method");
    }
}


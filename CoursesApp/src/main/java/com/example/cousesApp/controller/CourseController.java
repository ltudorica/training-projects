package com.example.cousesApp.controller;

import com.example.cousesApp.service.CourseService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@AllArgsConstructor
@Controller
public class CourseController {

    @Autowired
    @Qualifier("badCourseService")
    private final CourseService courseService;

    public void printCourseDetails() {
        courseService.printDetailsAboutCourse();
    }
}

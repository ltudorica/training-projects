package com.example.cousesApp.model;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class TechCourse implements Course {
    @Value("tech.course.name")
    private String name;

    @Override
    public void courseDetails() {
        System.out.println("This is a tech / " + this.name + " course");
    }
}

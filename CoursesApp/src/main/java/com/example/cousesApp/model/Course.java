package com.example.cousesApp.model;

public interface Course {
    void courseDetails();
    String getName();
}

package com.example.cousesApp;

import com.example.cousesApp.config.AppConfig;
import com.example.cousesApp.controller.CourseController;
import com.example.cousesApp.model.Course;
import com.example.cousesApp.model.TechCourse;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com/example/cousesApp")
public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        Course course = context.getBean("getTechCourse", TechCourse.class);
        System.out.println(course.getName());

        CourseController controller = (CourseController) context.getBean("courseController");
        controller.printCourseDetails();
    }
}

package com.example.cousesApp.service;

import com.example.cousesApp.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BadCourseService implements CourseService{

    public BadCourseService() {
        System.out.println("testing bad");
    }

    @Autowired
    private Course course;

    public void printDetailsAboutCourse() {
        System.out.println("This is a random message");
    }
}

package com.example.cousesApp.service;

import com.example.cousesApp.model.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class GoodCourseService implements CourseService{

    public GoodCourseService() {
        System.out.println("testing good");
    }

    @Autowired
    @Qualifier("techCourse")
    private Course course;

    public void printDetailsAboutCourse() {
        course.courseDetails();
    }

}

package com.example.cousesApp.config;

import com.example.cousesApp.model.Course;
import com.example.cousesApp.model.TechCourse;
import com.example.cousesApp.service.BadCourseService;
import com.example.cousesApp.service.CourseService;
import com.example.cousesApp.service.GoodCourseService;
import org.springframework.context.annotation.Bean;


public class AppConfig {

    @Bean
    public Course getTechCourse() {
        return new TechCourse();
    }

    @Bean
    public CourseService getBadService() {
        return new BadCourseService();
    }

    @Bean
    public CourseService getGoodService() {
        return new GoodCourseService();
    }


}

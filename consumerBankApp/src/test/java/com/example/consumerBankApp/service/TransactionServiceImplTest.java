package com.example.consumerBankApp.service;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionBetweenDatesDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionEditDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionResponseDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransferDTO;
import com.example.consumerBankApp.entity.Account;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.entity.Transaction;
import com.example.consumerBankApp.exception.AccountNotExistException;
import com.example.consumerBankApp.exception.CustomerNotFoundException;
import com.example.consumerBankApp.exception.InsufficientFundsException;
import com.example.consumerBankApp.exception.TransactionNotFoundException;
import com.example.consumerBankApp.repo.AccountRepository;
import com.example.consumerBankApp.repo.CustomerRepository;
import com.example.consumerBankApp.repo.TransactionRepository;
import com.example.consumerBankApp.service.impl.TransactionServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceImplTest {

    @Mock
    TransactionRepository transactionRepository;

    @Mock
    AccountRepository accountRepository;

    @Mock
    CustomerRepository customerRepository;

    @InjectMocks
    TransactionServiceImpl transactionService;

    TransferDTO transferDTO;
    TransactionResponseDTO transactionResponseDTO;
    List<TransactionResponseDTO> transactionResponseDTOList;
    TransactionEditDTO transactionEditDTO;
    Account account;
    Transaction transaction;
    List<Transaction> transactions;
    List<Account> accounts;
    BeneficiaryRequestDTO beneficiaryRequestDTO;

    @BeforeEach
    public void setUp() {
        setUpTransferDTO();
        setUpTransactionResponseDTO();
        setUpTransactionResponseDTOList();
        setUpTransactionEditDTO();
        setUpAccount();
        setUpAccounts();
        setUpTransaction();
        setUpTransactions();
        setUpBeneficiaryRequestDTO();
    }

    private void setUpTransferDTO() {
        transferDTO = new TransferDTO();
        transferDTO.setSenderAccountNumber(123L);
        transferDTO.setReceiverAccountNumber(321L);
        transferDTO.setAmount(100.5);
    }

    private void setUpTransactionResponseDTO() {
        transactionResponseDTO = new TransactionResponseDTO();
        transactionResponseDTO.setTransactionType("Credit");
        Account acc = new Account();
        Customer c = new Customer();
        c.setCustomerId(1);
        acc.setCustomer(c);
        transactionResponseDTO.setAccount(acc);
        transactionResponseDTO.setTransactionNumber(1);
        transactionResponseDTO.setAmount(10.5);
    }

    private void setUpTransactionResponseDTOList() {
        transactionResponseDTOList = new ArrayList<>();
        transactionResponseDTOList.add(transactionResponseDTO);
    }

    private void setUpTransactionEditDTO() {
        transactionEditDTO = new TransactionEditDTO();
        transactionEditDTO.setTransactionId(2);
        transactionEditDTO.setAmount(1000.2);
    }

    private void setUpAccount() {
        account = new Account();
        account.setAccountId(1);
        account.setBalance(1000.3);
        Customer c = new Customer();
        c.setCustomerId(1);
        account.setCustomer(c);
    }
    private void setUpAccounts() {
        accounts = new ArrayList<>();
        Customer customer1 = new Customer();
        customer1.setCustomerId(10);
        Account acc1 = new Account();
        acc1.setCustomer(customer1);
        accounts.add(account);
        accounts.add(acc1);
    }

    private void setUpTransaction() {
        transaction = new Transaction();
        BeanUtils.copyProperties(transactionResponseDTO, transaction);
    }

    private void setUpBeneficiaryRequestDTO() {
        beneficiaryRequestDTO = new BeneficiaryRequestDTO();
        beneficiaryRequestDTO.setBeneficiaryCustomerId(1);
        beneficiaryRequestDTO.setBenefactorCustomerId(2);
    }

    private void setUpTransactions() {
        transactions = new ArrayList<>();
        transactionResponseDTOList.forEach(Transaction -> {
            Transaction transaction = new Transaction();
            BeanUtils.copyProperties(transactionResponseDTO, transaction);
            transactions.add(transaction);
        });
    }

    @Test
    @Disabled
    @DisplayName("Transfer funds")
    public void transferFundsTest() {
        Customer c = new Customer();
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.of(c));
        when(accountRepository.findByAccountNumber(transferDTO.getSenderAccountNumber())).thenReturn(account);
        when(accountRepository.findByAccountNumber(transferDTO.getReceiverAccountNumber())).thenReturn(account);
        account.setBalance(10000);

        String result = transactionService.transferFunds(transferDTO, beneficiaryRequestDTO);
        assertEquals("Transaction completed", result);
    }

//    @Test
//    @DisplayName("Transfer funds - Exception thrown - TransactionWithNumberAlreadyExist")
//    public void transferFunds_TransactionWithNumberAlreadyExistTest() {
//        String message = "The transaction number is already in use";
//        when(transactionRepository.findByTransactionNumber(transferDTO.getTransactionNumber())).thenReturn(transactions);
//
//        assertThrows(TransactionWithNumberAlreadyExist.class, () -> transactionService.transferFunds(transferDTO),
//                message);
//    }

    @Test
    @DisplayName("Transfer funds - Exception thrown - AccountNotExistException")
    public void transferFunds_AccountNotExistExceptionTest() {
        Customer c = new Customer();
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.of(c));
        String message = "One of the accounts / or both cannot be found. Please check and try again.";
        when(accountRepository.findByAccountNumber(any(Long.class))).thenReturn(null);

        assertThrows(AccountNotExistException.class, () -> transactionService.transferFunds(transferDTO, beneficiaryRequestDTO),
                message);
    }

    @Test
    @Disabled
    @DisplayName("Transfer funds - Exception thrown - InsufficientFundsException")
    public void transferFunds_InsufficientFundsExceptionTest() {
        Customer c = new Customer();
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.of(c));
        when(accountRepository.findByAccountNumber(any(Long.class))).thenReturn(account);
        account.setBalance(1);
        String message = "Insufficient funds for making this transaction. Balance is " + account.getBalance();
        account.setBalance(1);

        assertThrows(InsufficientFundsException.class, () -> transactionService.transferFunds(transferDTO, beneficiaryRequestDTO),
                message);
    }

    @Test
    @DisplayName("Get all transactions")
    public void getTransactionsTest() {
        when(transactionRepository.findAll()).thenReturn(transactions);
        List<TransactionResponseDTO> dtos = new ArrayList<>();
        transactions.forEach(Transaction -> {
            TransactionResponseDTO t = convertTransactionToDTO(Transaction);
            dtos.add(t);
        });

        List<TransactionResponseDTO> result = transactionService.getTransactions();
        assertEquals(dtos, result);
    }

    @Test
    @DisplayName("Get transaction by id")
    public void getTransactionTest() {
        when(transactionRepository.findById(transaction.getTransactionId())).thenReturn(Optional.of(transaction));

        TransactionResponseDTO result = transactionService.getTransaction(transaction.getTransactionId());
        assertEquals(convertTransactionToDTO(transaction), result);
    }

    @Test
    @DisplayName("Get transaction by id - Exception thrown - TransactionNotFoundException")
    public void getTransaction_TransactionNotFoundExceptionTest() {
        when(transactionRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        String message = "Transaction cannot be found";

        assertThrows(TransactionNotFoundException.class, () -> transactionService.getTransaction(any(Integer.class)),
                message);
    }

    @Test
    @DisplayName("Update transaction")
    public void updateTransactionDataTest() {
        when(transactionRepository.findById(transactionEditDTO.getTransactionId())).thenReturn(Optional.of(transaction));

        TransactionResponseDTO result = transactionService.updateTransactionData(transactionEditDTO);
        assertEquals(convertTransactionToDTO(transaction), result);
    }

    @Test
    @DisplayName("Update transaction - Exception thrown - TransactionNotFoundException")
    public void updateTransactionData_TransactionNotFoundExceptionTest() {
        when(transactionRepository.findById(any(Integer.class))).thenReturn(Optional.empty());
        String message = "Transaction cannot be found";

        assertThrows(TransactionNotFoundException.class, () -> transactionService.updateTransactionData(transactionEditDTO),
                message);
    }

    @Test
    @DisplayName("Delete transaction")
    public void deleteTest() {
        when(transactionRepository.findById(transaction.getTransactionId())).thenReturn(Optional.of(transaction));

        String result = transactionService.delete(transaction.getTransactionId());
        assertEquals("Transaction was deleted", result);
    }

    @Test
    @DisplayName("Delete transaction - Exception thrown - TransactionNotFoundException")
    public void delete_TransactionNotFoundExceptionTest() {
        String message = "Transaction cannot be found!";
        when(transactionRepository.findById(transaction.getTransactionId())).thenReturn(Optional.empty());

        assertThrows(TransactionNotFoundException.class, () -> transactionService.delete(transaction.getTransactionId()),
                message);
    }

    @Test
    @DisplayName("Get Transactions for account number")
    public void getTransactionsForAccountNumberTest() {
        when(transactionRepository.findById(transaction.getTransactionId())).thenReturn(Optional.of(transaction));

        String result = transactionService.delete(transaction.getTransactionId());
        assertEquals("Transaction was deleted", result);
    }

    @Test
    @DisplayName("Get Transactions for account number - Exception thrown - AccountNotExistException")
    public void getTransactionsForAccountNumber_AccountNotExistExceptionTest() {
        String message = "The account cannot be found";
        when(accountRepository.findByAccountNumber(any(Long.class))).thenReturn(null);

        assertThrows(AccountNotExistException.class, () -> transactionService.getTransactionsForAccountNumber(any(Long.class)),
                message);
    }

    @Test
    @DisplayName("Get Transactions by customer Id")
    public void getTransactionsForCustomerTest() {
        Customer c = new Customer();
        c.setCustomerId(2);
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.of(c));
        when(accountRepository.findByCustomer_CustomerId(c.getCustomerId())).thenReturn(accounts);
        when(transactionRepository.findByAccount_AccountId(any(Integer.class))).thenReturn(transactions);

        List<TransactionResponseDTO> expected = new ArrayList<>();
        transactions.forEach(Transaction -> {
            TransactionResponseDTO t = convertTransactionToDTO(Transaction);
            expected.add(t);
        });

        List<TransactionResponseDTO> result = transactionService.getTransactionsForCustomer(c.getCustomerId());
        assertEquals(expected, result);
    }

    @Test
    @DisplayName("Get Transactions by customer Id - Exception thrown - CustomerNotFoundException")
    public void getTransactionsForCustomer_CustomerNotFoundExceptionTest() {
        String message = "The customer cannot be found!";
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        assertThrows(CustomerNotFoundException.class, () -> transactionService.getTransactionsForCustomer(any(Integer.class)),
                message);
    }

    @Test
    @DisplayName("Get Transactions by month - Exception thrown - CustomerNotFoundException")
    public void getTransactionsByMonth_CustomerNotFoundExceptionTest() {
        String message = "The customer cannot be found!";
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        assertThrows(CustomerNotFoundException.class, () -> transactionService.getTransactionsByMonth(any(Integer.class), 2),
                message);
    }

    @Test
    @DisplayName("Get Transactions between dates - Exception thrown - CustomerNotFoundException")
    public void getTransactionsBetweenDates_CustomerNotFoundExceptionTest() {
        String message = "The customer cannot be found!";
        TransactionBetweenDatesDTO t = new TransactionBetweenDatesDTO();
        t.setCustomerId(2);
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        assertThrows(CustomerNotFoundException.class, () -> transactionService.getTransactionsBetweenDates(t),
                message);
    }

    private TransactionResponseDTO convertTransactionToDTO(Transaction transaction) {
        TransactionResponseDTO t = new TransactionResponseDTO();
        BeanUtils.copyProperties(transaction, t);
        return t;
    }
}

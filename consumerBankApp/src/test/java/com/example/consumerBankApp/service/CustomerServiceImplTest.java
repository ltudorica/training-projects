package com.example.consumerBankApp.service;

import com.example.consumerBankApp.dto.customerDTO.CustomerIdRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponse;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponseDTO;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.exception.CustomerAlreadyExistException;
import com.example.consumerBankApp.exception.CustomerNotFoundException;
import com.example.consumerBankApp.repo.CustomerRepository;
import com.example.consumerBankApp.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTest {

    @Mock
    CustomerRepository customerRepository;

    @InjectMocks
    CustomerServiceImpl customerServiceImpl;

    CustomerRequestDTO customerRequestDTO;
    List<Customer> customerList;
    Customer customer;
    List<CustomerResponse> customerResponses;
    CustomerIdRequestDTO customerIdRequestDTO;

    @BeforeEach
    public void setUp() {
        setUpCustomerRequestDTO();
        setUpCustomerAndList();
        setUpCustomerResponsesList();
        setUpCustomerIdRequestDTO();
    }

    private void setUpCustomerRequestDTO() {
        customerRequestDTO = new CustomerRequestDTO();
        customerRequestDTO.setCustomerName("Ana");
        customerRequestDTO.setPhoneNo("123");
        customerRequestDTO.setAddress("Bucharest");
        customerRequestDTO.setCustomerId(1);
    }

    private void setUpCustomerAndList() {
        customerList = new ArrayList<>();
        customer = new Customer();
        customer.setCustomerId(1);
        customer.setCustomerName("Ana");
        customer.setAddress("Bucharest");
        customer.setPhoneNo("123");
        customerList.add(customer);
    }

    private void setUpCustomerResponsesList() {
        customerResponses = new ArrayList<>();
        customerResponses.add(new CustomerResponse() {
            @Override
            public String getCustomerName() {
                return "Ana";
            }

            @Override
            public String getPhoneNo() {
                return "123";
            }

            @Override
            public String getAddress() {
                return "test";
            }
        });
    }

    private void setUpCustomerIdRequestDTO() {
        customerIdRequestDTO = new CustomerIdRequestDTO();
        customerIdRequestDTO.setCustomerId(1);
    }

    @Test
    @DisplayName("Save customer data")
    public void saveCustomerDataTest() {
        //context
        when(customerRepository.save(any(Customer.class))).thenAnswer(i -> {
            Customer customer = i.getArgument(0);
            customer.setCustomerId(1);
            return customer;
        });

        //event
        String result = customerServiceImpl.saveCustomerData(customerRequestDTO);
        assertEquals("Data saved successfully for customer: " + customerRequestDTO.getCustomerName(), result);
    }

    @Test
    @DisplayName("Save customer data - Exception thrown - CustomerAlreadyExistException")
    public void saveCustomerData_CustomerAlreadyExistExceptionTest() {
        String message = "This phone number is used by another customer";
        when(customerRepository.findCustomerByPhoneNo(any(String.class))).thenReturn(customer);

        assertThrows(CustomerAlreadyExistException.class, () -> customerServiceImpl.saveCustomerData(customerRequestDTO),
                message);
    }

    @Test
    @DisplayName("Get all customers")
    public void getCustomerDetailsTest() {
        when(customerRepository.findAll()).thenReturn(customerList);
        List<CustomerResponseDTO> customerResponseDTOList = new ArrayList<>();
        customerList.forEach(Customer -> {
            customerResponseDTOList.add(convertCustomerInCustomerDTO(Customer));
        });

        List<CustomerResponseDTO> result = customerServiceImpl.getCustomerDetails();
        assertEquals(customerResponseDTOList, result);
    }

    @Test
    @DisplayName("Get customer by id")
    public void getCustomerDetailsByIdTest() {
        when(customerRepository.findById(customer.getCustomerId())).thenReturn(Optional.of(customer));

        CustomerResponseDTO result = customerServiceImpl.getCustomerDetails(customer.getCustomerId());
        assertEquals(convertCustomerInCustomerDTO(customer), result);
    }

    @Test
    @DisplayName("Get customer by id - Exception thrown - CustomerNotFoundException")
    public void getCustomerDetailsById_CustomerNotFoundExceptionTest() {
        String message = "There is no customer for id 1";
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        assertThrows(CustomerNotFoundException.class, () -> customerServiceImpl.getCustomerDetails(1),
                message);
    }

    @Test
    @DisplayName("Get customer by name")
    public void getCustomerDetailsByNameTest() {
        when(customerRepository.findByCustomerNameLike(customer.getCustomerName())).thenReturn(customerResponses);

        List<CustomerResponse> result = customerServiceImpl.getCustomerDetails(customer.getCustomerName());
        assertEquals(customerResponses, result);
    }

    @Test
    @DisplayName("Get customer by id - Exception thrown - CustomerNotFoundException")
    public void getCustomerDetailsByName_CustomerNotFoundExceptionTest() {
        String message = "No customers found under Ana name";
        List<CustomerResponse> customersToTest = new ArrayList<>();
        when(customerRepository.findByCustomerNameLike(any(String.class))).thenReturn(customersToTest);

        assertThrows(CustomerNotFoundException.class, () -> customerServiceImpl.getCustomerDetails("Ana"),
                message);
    }

    @Test
    @DisplayName("Update customer")
    public void updateCustomerTest() {
        when(customerRepository.findById(customerRequestDTO.getCustomerId())).thenReturn(Optional.of(customer));

        CustomerResponseDTO result = customerServiceImpl.updateCustomer(customerRequestDTO);
        assertEquals(convertCustomerInCustomerDTO(customer), result);
    }

    @Test
    @DisplayName("Update customer - Exception thrown - customer not found")
    public void updateCustomer_CustomerNotFoundExceptionTest() {
        String message = "There is no customer for id " + customerRequestDTO.getCustomerId();
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        CustomerNotFoundException thrown = assertThrows(CustomerNotFoundException.class, () ->
                customerServiceImpl.updateCustomer(customerRequestDTO));
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Update customer - Exception thrown - dto does not have customer Id")
    public void updateCustomer_CustomerNotFoundExceptionTest_NoIdInDTO() {
        customerRequestDTO.setCustomerId(null);
        String message = "There is no id to search, please try again!";

        CustomerNotFoundException thrown = assertThrows(CustomerNotFoundException.class, () ->
                        customerServiceImpl.updateCustomer(customerRequestDTO));
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Update customer - Exception thrown - phone no in use")
    public void updateCustomer_CustomerAlreadyExistExceptionTest_() {
        String message = "This phone number is used by another customer";
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.of(customer));
        when(customerRepository.findCustomerByPhoneNo(any(String.class))).thenReturn(customer);

        CustomerAlreadyExistException thrown = assertThrows(CustomerAlreadyExistException.class, () ->
                customerServiceImpl.updateCustomer(customerRequestDTO));
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Delete customer")
    public void deleteCustomerTest() {
        when(customerRepository.findById(customerIdRequestDTO.getCustomerId())).thenReturn(Optional.of(customer));

        String result = customerServiceImpl.deleteCustomer(customerIdRequestDTO);
        assertEquals("Customer deleted successfully", result);
    }

    @Test
    @DisplayName("Delete customer - Exception thrown - CustomerNotFoundException")
    public void deleteCustomer_CustomerNotFoundExceptionTest() {
        String message = "There is no customer for id " + customerIdRequestDTO.getCustomerId();
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        CustomerNotFoundException thrown = assertThrows(CustomerNotFoundException.class, () ->
                customerServiceImpl.deleteCustomer(customerIdRequestDTO));
        assertEquals(thrown.getMessage(), message);
    }

    private CustomerResponseDTO convertCustomerInCustomerDTO(Customer customer) {
        CustomerResponseDTO responseDTO = new CustomerResponseDTO();
        BeanUtils.copyProperties(customer, responseDTO);
        return responseDTO;
    }


}

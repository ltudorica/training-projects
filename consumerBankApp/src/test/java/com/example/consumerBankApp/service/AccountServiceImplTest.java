package com.example.consumerBankApp.service;

import com.example.consumerBankApp.dto.accountDTO.*;
import com.example.consumerBankApp.entity.Account;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.exception.AccountAlreadyExistException;
import com.example.consumerBankApp.exception.AccountNotExistException;
import com.example.consumerBankApp.exception.CustomerNotFoundException;
import com.example.consumerBankApp.repo.AccountRepository;
import com.example.consumerBankApp.repo.CustomerRepository;
import com.example.consumerBankApp.service.impl.AccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceImplTest {

    @Mock
    AccountRepository accountRepository;

    @Mock
    CustomerRepository customerRepository;

    @InjectMocks
    AccountServiceImpl accountService;

    AccountRequestDTO accountRequestDTO;
    AccountResponseDTO accountResponseDTO;
    List<AccountResponseDTO> accountResponseDTOs;
    AccountResponse accountResponse;
    AccountEditDTO accountEditDTO;
    AccountIdDTO accountIdDTO;
    Account account;
    Customer customer;

    @BeforeEach
    public void setUp() {
        setUpCustomer();
        setUpAccountRequestDTO();
        setUpAccountResponseDTOAndListOfResponses();
        setUpAccountResponse();
        setUpAccountEditDTO();
        setUpAccountIdDTO();
        setUpAccount();
    }

    private void setUpAccountRequestDTO() {
        accountRequestDTO = new AccountRequestDTO();
        accountRequestDTO.setAccountId(1);
        accountRequestDTO.setAccountNumber(123L);
        accountRequestDTO.setAccountType("CA");
        accountRequestDTO.setBalance(1000.5);

        accountRequestDTO.setCustomer(customer);
    }

    private void setUpAccountResponseDTOAndListOfResponses() {
        accountResponseDTO = new AccountResponseDTO();
        BeanUtils.copyProperties(accountRequestDTO, accountResponseDTO);

        accountResponseDTOs = new ArrayList<>();
        accountResponseDTOs.add(accountResponseDTO);
    }

    private void setUpAccountResponse() {
        accountResponse = new AccountResponse(123L, 100.5, customer, "CA");
    }

    private void setUpAccountEditDTO() {
        accountEditDTO = new AccountEditDTO();
        BeanUtils.copyProperties(accountRequestDTO, accountEditDTO);
    }

    private void setUpAccountIdDTO() {
        accountIdDTO = new AccountIdDTO();
        accountIdDTO.setAccountId(1);
    }

    private void setUpCustomer() {
        customer = new Customer();
        customer.setCustomerId(1);
    }

    private void setUpAccount() {
        account = new Account();
        BeanUtils.copyProperties(accountRequestDTO, account);
    }

    @Test
    @DisplayName("Save Account Data")
    public void saveAccountDataTest() {
        when(customerRepository.findById(accountRequestDTO.getAccountId())).thenReturn(Optional.of(customer));

        AccountResponseDTO accountResponseDTO1 = new AccountResponseDTO();
        BeanUtils.copyProperties(account, accountResponseDTO1);

        AccountResponseDTO result = accountService.saveAccountData(accountRequestDTO);
        assertEquals(accountResponseDTO1.getAccountId(), result.getAccountId());
        assertEquals(accountResponseDTO1.getAccountNumber(), result.getAccountNumber());
    }

    @Test
    @DisplayName("Save Account Data - Exception thrown - CustomerNotFoundException")
    public void saveAccountData_CustomerNotFoundExceptionTest() {
        String message = "Customer doesn't exist for the id " + accountRequestDTO.getCustomer().getCustomerId() +
                ", please assign the account to an existing customer!";
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        CustomerNotFoundException thrown = assertThrows(CustomerNotFoundException.class, () ->
                accountService.saveAccountData(accountRequestDTO));
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Save Account Data - Exception thrown - AccountAlreadyExistException")
    public void saveAccountData_AccountAlreadyExistExceptionTest() {
        String message = "Cannot assign this account, it is already in use!";
        when(customerRepository.findById(any(Integer.class))).thenReturn(Optional.of(customer));
        when(accountRepository.findByAccountNumber(any(Long.class))).thenReturn(account);

        AccountAlreadyExistException thrown = assertThrows(AccountAlreadyExistException.class, () ->
                accountService.saveAccountData(accountRequestDTO));
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Get all accounts")
    public void getAccountsDetailsTest() {
        when(accountRepository.findAccounts()).thenReturn(accountResponseDTOs);

        List<AccountResponseDTO> result = accountService.getAccountsDetails();
        assertEquals(accountResponseDTOs, result);
    }

    @Test
    @DisplayName("Get all accounts - Exception thrown - AccountNotExistException")
    public void getAccountsDetails_AccountNotExistExceptionTest() {
        String message = "No accounts found";
        List<AccountResponseDTO> acc = new ArrayList<>();
        when(accountRepository.findAccounts()).thenReturn(acc);

        AccountNotExistException thrown = assertThrows(AccountNotExistException.class, () ->
                accountService.getAccountsDetails());
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Get account by account number")
    public void findAccountByAccountNumberTest() {
        when(accountRepository.findByAccountNumber(any(Long.class))).thenReturn(account);
        when(accountRepository.findDTOByAccountNumber(any(Long.class))).thenReturn(accountResponse);

        AccountResponse result = accountService.findAccountByAccountNumber(accountRequestDTO.getAccountNumber());
        assertEquals(accountResponse.getAccountNumber(), result.getAccountNumber());
        assertEquals(accountResponse.getAccountType(), result.getAccountType());
        assertEquals(accountResponse.getBalance(), result.getBalance());
        assertEquals(accountResponse.getCustomer(), result.getCustomer());
    }

    @Test
    @DisplayName("Get account by account number - Exception thrown - AccountNotExistException")
    public void findAccountByAccountNumber_AccountNotExistExceptionTest() {
        Long accountNumber = 123L;
        String message = "There is no account found for account number " + accountNumber;
        when(accountRepository.findByAccountNumber(any(Long.class))).thenReturn(null);

        AccountNotExistException thrown = assertThrows(AccountNotExistException.class, () ->
                accountService.findAccountByAccountNumber(accountNumber));
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Update account")
    public void updateAccountTest() {
        when(accountRepository.findById(accountIdDTO.getAccountId())).thenReturn(Optional.of(account));
        when(accountRepository.save(account)).thenReturn(account);

        AccountResponse expected = new AccountResponse(account.getAccountNumber(), account.getBalance(), account.getCustomer(), account.getAccountType());

        AccountResponse result = accountService.updateAccount(accountEditDTO);
        assertEquals(expected.getAccountNumber(), result.getAccountNumber());
        assertEquals(expected.getAccountType(), result.getAccountType());
        assertEquals(expected.getBalance(), result.getBalance());
        assertEquals(expected.getCustomer(), result.getCustomer());
    }

    @Test
    @DisplayName("Update account - Exception thrown - AccountNotExistException")
    public void updateAccount_AccountNotExistExceptionTest() {
        String message = "There is no account found for id " + accountEditDTO.getAccountId();
        when(accountRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        AccountNotExistException thrown = assertThrows(AccountNotExistException.class, () ->
                accountService.updateAccount(accountEditDTO));
        assertEquals(thrown.getMessage(), message);
    }

    @Test
    @DisplayName("Delete account")
    public void deleteAccountTest() {
        when(accountRepository.findById(accountRequestDTO.getAccountId())).thenReturn(Optional.of(account));

        String result = accountService.deleteAccount(accountIdDTO);
        assertEquals("Account deleted successfully", result);
    }

    @Test
    @DisplayName("Delete account - Exception thrown - AccountNotExistException")
    public void deleteAccount_AccountNotExistExceptionTest() {
        String message = "There is no account found for id " + accountIdDTO.getAccountId();
        when(accountRepository.findById(any(Integer.class))).thenReturn(Optional.empty());

        AccountNotExistException thrown = assertThrows(AccountNotExistException.class, () ->
                accountService.deleteAccount(accountIdDTO));
        assertEquals(thrown.getMessage(), message);
    }
}

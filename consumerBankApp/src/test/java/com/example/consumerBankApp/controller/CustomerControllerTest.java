package com.example.consumerBankApp.controller;

import com.example.consumerBankApp.dto.customerDTO.CustomerIdRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponse;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponseDTO;
import com.example.consumerBankApp.exception.CustomerAlreadyExistException;
import com.example.consumerBankApp.exception.CustomerNotFoundException;
import com.example.consumerBankApp.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {

    @Mock
    CustomerService customerService;

    @InjectMocks //like autowiring the mocked service
    CustomerController customerController;

    CustomerRequestDTO customerRequestDTO;
    CustomerResponseDTO customerResponseDTO;
    List<CustomerResponseDTO> customers;
    List<CustomerResponse> customerResponses;
    CustomerIdRequestDTO customerIdRequestDTO;

    @BeforeEach
    public void setUp() {
        setUpCustomerRequestDTO();
        customerResponseDTO = new CustomerResponseDTO();
        customers = new ArrayList<>();
        customerResponses = new ArrayList<>();
        customerIdRequestDTO = new CustomerIdRequestDTO();
        customerIdRequestDTO.setCustomerId(1);
    }

    private void setUpCustomerRequestDTO() {
        customerRequestDTO = new CustomerRequestDTO();
        customerRequestDTO.setCustomerName("Ana");
        customerRequestDTO.setPhoneNo("1234");
        customerRequestDTO.setAddress("Bucharest");
    }

    @Test
    @DisplayName("Save Customer Data")
    public void saveCustomerDataTest() {
        //context - ruler
        when(customerService.saveCustomerData(customerRequestDTO))
                .thenReturn("Data saved successfully for customer: " + customerRequestDTO.getCustomerName());

        //event
        ResponseEntity<String> result = customerController.saveCustomerData(customerRequestDTO);
        assertEquals("Data saved successfully for customer: " + customerRequestDTO.getCustomerName(), result.getBody());
        assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
    }

    @Test
    @DisplayName("Save Customer Data - Exception handler - CustomerAlreadyExistException")
    public void saveCustomerData_CustomerAlreadyExistExceptionTest() {
        String message = "This phone number is used by another customer";
        when(customerService.saveCustomerData(customerRequestDTO)).thenThrow(new CustomerAlreadyExistException
                (message));
        assertThrows(CustomerAlreadyExistException.class, () ->
                customerController.saveCustomerData(customerRequestDTO), message);
    }

    @Test
    @DisplayName("Get all customers")
    public void getCustomersDetailsTest() {
        when(customerService.getCustomerDetails())
                .thenReturn(customers);

        ResponseEntity<List<CustomerResponseDTO>> result = customerController.getCustomersDetails();
        assertEquals(customers, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get customer by id")
    public void getCustomerDetailsTest() {
        Integer id = 1;
        when(customerService.getCustomerDetails(id))
                .thenReturn(customerResponseDTO);

        ResponseEntity<CustomerResponseDTO> result = customerController.getCustomerDetails(id);
        assertEquals(customerResponseDTO, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get customer by id - Exception handler - CustomerNotFoundException")
    public void getCustomerDetails_CustomerNotFoundExceptionTest() {
        String message = "There is no customer for id 1";
        when(customerService.getCustomerDetails(any(Integer.class))).thenThrow(new CustomerNotFoundException
                (message));
        assertThrows(CustomerNotFoundException.class, () ->
                customerController.getCustomerDetails(any(Integer.class)), message);
    }


    @Test
    @DisplayName("Get customer by name")
    public void getCustomerDetailsByNameTest() {
        String name = "Ana";
        when(customerService.getCustomerDetails(name))
                .thenReturn(customerResponses);

        ResponseEntity<List<CustomerResponse>> result = customerController.getCustomerDetails(name);
        assertEquals(customerResponses, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get customer by name - Exception handler - CustomerNotFoundException")
    public void getCustomerDetailsByName_CustomerNotFoundExceptionTest() {
        String message = "No customers found under Ana name";
        when(customerService.getCustomerDetails("Ana")).thenThrow(new CustomerNotFoundException
                (message));
        assertThrows(CustomerNotFoundException.class, () ->
                customerController.getCustomerDetails("Ana"), message);
    }

    @Test
    @DisplayName("Update customer")
    public void updateCustomerTest() {
        when(customerService.updateCustomer(customerRequestDTO))
                .thenReturn(customerResponseDTO);

        ResponseEntity<CustomerResponseDTO> result = customerController.updateCustomer(customerRequestDTO);
        assertEquals(customerResponseDTO, result.getBody());
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    @DisplayName("Update customer - Exception handler - CustomerNotFoundException - requestDTO has no id")
    public void updateCustomer_CustomerNotFoundExceptionTest_NoIdInDTO() {
        String message = "There is no id to search, please try again!";
        when(customerService.updateCustomer(customerRequestDTO)).thenThrow(new CustomerNotFoundException
                (message));
        assertThrows(CustomerNotFoundException.class, () ->
                customerController.updateCustomer(customerRequestDTO), message);
    }

    @Test
    @DisplayName("Update customer - Exception handler - CustomerNotFoundException - customer not found")
    public void updateCustomer_CustomerNotFoundExceptionTest() {
        String message = "There is no customer for id " + customerRequestDTO.getCustomerId();
        when(customerService.updateCustomer(customerRequestDTO)).thenThrow(new CustomerNotFoundException
                (message));
        assertThrows(CustomerNotFoundException.class, () ->
                customerController.updateCustomer(customerRequestDTO), message);
    }

    @Test
    @DisplayName("Update customer - Exception handler - CustomerAlreadyExistException - phone no in use")
    public void updateCustomer_CustomerAlreadyExistExceptionTest() {
        String message = "This phone number is used by another customer";
        when(customerService.updateCustomer(customerRequestDTO)).thenThrow(new CustomerAlreadyExistException
                (message));
        assertThrows(CustomerAlreadyExistException.class, () ->
                customerController.updateCustomer(customerRequestDTO), message);
    }

    @Test
    @DisplayName("Delete customer")
    public void deleteCustomerTest() {
        when(customerService.deleteCustomer(customerIdRequestDTO))
                .thenReturn("Customer deleted successfully");

        ResponseEntity<String> result = customerController.deleteCustomer(customerIdRequestDTO);
        assertEquals("Customer deleted successfully", result.getBody());
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    @DisplayName("Delete customer - Exception handler - CustomerNotFoundException")
    public void deleteCustomer_CustomerNotFoundExceptionTest() {
        String message = "There is no customer for id " + customerIdRequestDTO.getCustomerId();
        when(customerService.deleteCustomer(customerIdRequestDTO)).thenThrow(new CustomerNotFoundException
                (message));
        assertThrows(CustomerNotFoundException.class, () ->
                customerController.deleteCustomer(customerIdRequestDTO), message);
    }
}

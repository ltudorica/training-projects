package com.example.consumerBankApp.controller;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionBetweenDatesDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionEditDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionResponseDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransferDTO;
import com.example.consumerBankApp.entity.Account;
import com.example.consumerBankApp.exception.*;
import com.example.consumerBankApp.service.TransactionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Mock
    TransactionService transactionService;

    @InjectMocks
    TransactionController transactionController;

    TransferDTO transferDTO;
    TransactionResponseDTO transactionResponseDTO;
    List<TransactionResponseDTO> transactionResponseDTOList;
    TransactionEditDTO transactionEditDTO;
    TransactionBetweenDatesDTO transactionBetweenDatesDTO;
    BeneficiaryRequestDTO beneficiaryRequestDTO;

    @BeforeEach
    public void setUp() {
        setUpTransferDTO();
        setUpTransactionResponseDTO();
        setUpTransactionResponseDTOList();
        setUpTransactionEditDTO();
        setUpTransactionBetweenDatesDTO();
        setUpBeneficiaryRequestDTO();
    }

    private void setUpTransferDTO() {
        transferDTO = new TransferDTO();
        transferDTO.setSenderAccountNumber(123L);
        transferDTO.setReceiverAccountNumber(321L);
        transferDTO.setAmount(100.5);
    }

    private void setUpTransactionResponseDTO() {
        transactionResponseDTO = new TransactionResponseDTO();
        transactionResponseDTO.setTransactionType("Credit");
        transactionResponseDTO.setAccount(new Account());
        transactionResponseDTO.setTransactionNumber(1);
        transactionResponseDTO.setAmount(10.5);
    }

    private void setUpTransactionResponseDTOList() {
        transactionResponseDTOList = new ArrayList<>();
        transactionResponseDTOList.add(transactionResponseDTO);
    }

    private void setUpTransactionEditDTO() {
        transactionEditDTO = new TransactionEditDTO();
        transactionEditDTO.setTransactionId(2);
        transactionEditDTO.setAmount(1000.2);
    }

    private void setUpTransactionBetweenDatesDTO() {
        transactionBetweenDatesDTO = new TransactionBetweenDatesDTO();
        transactionBetweenDatesDTO.setCustomerId(1);
        transactionBetweenDatesDTO.setStartDate(LocalDate.of(2021, 12, 20));
        transactionBetweenDatesDTO.setEndDate(LocalDate.of(2021, 12, 19));
    }

    private void setUpBeneficiaryRequestDTO() {
        beneficiaryRequestDTO = new BeneficiaryRequestDTO();
        beneficiaryRequestDTO.setBeneficiaryCustomerId(1);
        beneficiaryRequestDTO.setBenefactorCustomerId(2);
    }

    @Test //to do add parameter TransactionRequestDTO
    @DisplayName("Transfer funds")
    public void transferFundsTest() {
        when(transactionService.transferFunds(transferDTO, beneficiaryRequestDTO)).thenReturn("Transaction completed");

        ResponseEntity<String> result = transactionController.transferFunds(transferDTO, beneficiaryRequestDTO);
        assertEquals("Transaction completed", result.getBody());
        assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
    }

//    @Test
//    @DisplayName("Transfer funds - Exception handler - Transaction With Number Already Exist")
//    public void transferFunds_TransactionWithNumberAlreadyExistTest() {
//        String message = "The transaction number is already in use";
//        when(transactionService.transferFunds(transferDTO)).thenThrow(new TransactionWithNumberAlreadyExist
//                (message));
//        assertThrows(TransactionWithNumberAlreadyExist.class, () ->
//                transactionController.transferFunds(transferDTO), message);
//    }

    @Test
    @DisplayName("Transfer funds - Exception handler - AccountNotExistException")
    public void transferFunds_AccountNotExistExceptionTest() {
        String message = "One of the accounts / or both cannot be found. Please check and try again.";
        when(transactionService.transferFunds(transferDTO, beneficiaryRequestDTO)).thenThrow(new AccountNotExistException
                (message));

        assertThrows(AccountNotExistException.class, () -> transactionController.transferFunds(transferDTO, beneficiaryRequestDTO), message);
    }

    @Test
    @DisplayName("Transfer funds - Exception handler - InsufficientFundsException")
    public void transferFunds_InsufficientFundsExceptionTest() {
        String message = "Insufficient funds for making this transaction. Balance is 100" ;
        when(transactionService.transferFunds(transferDTO, beneficiaryRequestDTO))
                .thenThrow(new InsufficientFundsException(message));

        assertThrows(InsufficientFundsException.class, () -> transactionController.transferFunds(transferDTO, beneficiaryRequestDTO), message);
    }

    @Test
    @DisplayName("Get all transactions")
    public void getTransactionsTest() {
        when(transactionService.getTransactions()).thenReturn(transactionResponseDTOList);

        ResponseEntity<List<TransactionResponseDTO>> result = transactionController.getTransactions();
        assertEquals(transactionResponseDTOList, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get transaction by id")
    public void getTransactionTest() {
        Integer transactionId = 1;
        when(transactionService.getTransaction(transactionId)).thenReturn(transactionResponseDTO);

        ResponseEntity<TransactionResponseDTO> result = transactionController.getTransaction(transactionId);
        assertEquals(transactionResponseDTO, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get transaction by id - Exception handler - TransactionNotFoundException")
    public void getTransaction_TransactionNotFoundExceptionTest() {
        String message = "Transaction cannot be found";
        Integer transactionId = 1;
        when(transactionService.getTransaction(transactionId)).thenThrow(new TransactionNotFoundException
                (message));

        assertThrows(TransactionNotFoundException.class, () -> transactionController.getTransaction(transactionId), message);
    }

    @Test
    @DisplayName("Update transaction")
    public void updateTransactionDataTest() {
        when(transactionService.updateTransactionData(transactionEditDTO)).thenReturn(transactionResponseDTO);

        ResponseEntity<TransactionResponseDTO> result = transactionController.updateTransactionData(transactionEditDTO);
        assertEquals(transactionResponseDTO, result.getBody());
        assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
    }

    @Test
    @DisplayName("Update transaction - Exception handler - TransactionNotFoundException")
    public void updateTransactionData_TransactionNotFoundExceptionTest() {
        String message = "Transaction cannot be found";
        when(transactionService.updateTransactionData(transactionEditDTO)).thenThrow(new TransactionNotFoundException
                (message));

        assertThrows(TransactionNotFoundException.class, () -> transactionController.updateTransactionData(transactionEditDTO), message);
    }

    @Test
    @DisplayName("Delete transaction")
    public void deleteTransactionTest() {
        Integer transactionId = 1;
        when(transactionService.delete(transactionId)).thenReturn("Transaction was deleted");

        ResponseEntity<String> result = transactionController.deleteTransaction(transactionId);
        assertEquals("Transaction was deleted", result.getBody());
        assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
    }

    @Test
    @DisplayName("Delete transaction - Exception handler - TransactionNotFoundException")
    public void deleteTransaction_TransactionNotFoundExceptionTest() {
        String message = "Transaction cannot be found";
        Integer transactionId = 1;
        when(transactionService.delete(transactionId)).thenThrow(new TransactionNotFoundException
                (message));

        assertThrows(TransactionNotFoundException.class, () -> transactionController.deleteTransaction(transactionId), message);
    }

    @Test
    @DisplayName("Get transactions for account number")
    public void getTransactionsForAccountNumberTest() {
        Long account = 111L;
        when(transactionService.getTransactionsForAccountNumber(account)).thenReturn(transactionResponseDTOList);

        ResponseEntity<List<TransactionResponseDTO>> result = transactionController.getTransactionsForAccountNumber(account);
        assertEquals(transactionResponseDTOList, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get transactions for account number - Exception handler - AccountNotExistException")
    public void getTransactionsForAccountNumber_AccountNotExistExceptionTest() {
        String message = "The account cannot be found";
        Long account = 111L;
        when(transactionService.getTransactionsForAccountNumber(account)).thenThrow(new AccountNotExistException
                (message));

        assertThrows(AccountNotExistException.class, () -> transactionController.getTransactionsForAccountNumber(account), message);
    }

    @Test
    @DisplayName("Get transactions by customer Id")
    public void getTransactionsForCustomerTest() {
        Integer customerId = 1;
        when(transactionService.getTransactionsForCustomer(customerId)).thenReturn(transactionResponseDTOList);

        ResponseEntity<List<TransactionResponseDTO>> result = transactionController.getTransactionsForCustomer(customerId);
        assertEquals(transactionResponseDTOList, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get transactions by customer Id - Exception handler - AccountNotExistException")
    public void getTransactionsForCustomer_CustomerNotFoundExceptionTest() {
        String message = "The customer cannot be found!";
        Integer customerId = 1;
        when(transactionService.getTransactionsForCustomer(customerId)).thenThrow(new CustomerNotFoundException
                (message));

        assertThrows(CustomerNotFoundException.class, () -> transactionController.getTransactionsForCustomer(customerId), message);
    }

    @Test
    @DisplayName("Get transactions by month")
    public void getTransactionsByMonthTest() {
        Integer customerId = 1;
        Integer month = 2;
        when(transactionService.getTransactionsByMonth(customerId, month)).thenReturn(transactionResponseDTOList);

        ResponseEntity<List<TransactionResponseDTO>> result = transactionController.getTransactionsByMonth(customerId, month);
        assertEquals(transactionResponseDTOList, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get transactions by month - Exception handler - AccountNotExistException")
    public void getTransactionsByMonth_CustomerNotFoundExceptionTest() {
        String message = "The customer cannot be found!";
        Integer customerId = 1;
        Integer month = 2;
        when(transactionService.getTransactionsByMonth(customerId, month)).thenThrow(new CustomerNotFoundException
                (message));

        assertThrows(CustomerNotFoundException.class, () -> transactionController.getTransactionsByMonth(customerId, month), message);
    }

    @Test
    @DisplayName("Get transactions between dates")
    public void getTransactionsBetweenDatesTest() {
        when(transactionService.getTransactionsBetweenDates(transactionBetweenDatesDTO)).thenReturn(transactionResponseDTOList);

        ResponseEntity<List<TransactionResponseDTO>> result = transactionController.getTransactionsBetweenDates(transactionBetweenDatesDTO);
        assertEquals(transactionResponseDTOList, result.getBody());
        assertEquals(HttpStatus.ACCEPTED, result.getStatusCode());
    }

    @Test
    @DisplayName("Get transactions between dates - Exception handler - AccountNotExistException")
    public void getTransactionsBetweenDatesTest_CustomerNotFoundExceptionTest() {
        String message = "The customer cannot be found!";
        when(transactionService.getTransactionsBetweenDates(transactionBetweenDatesDTO)).thenThrow(new CustomerNotFoundException
                (message));

        assertThrows(CustomerNotFoundException.class, () -> transactionController.getTransactionsBetweenDates(transactionBetweenDatesDTO), message);
    }
}

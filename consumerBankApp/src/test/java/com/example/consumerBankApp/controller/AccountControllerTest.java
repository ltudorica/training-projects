package com.example.consumerBankApp.controller;

import com.example.consumerBankApp.dto.accountDTO.*;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountControllerTest {

    @Mock
    AccountService accountService;

    @InjectMocks
    AccountController accountController;

    AccountRequestDTO accountRequestDTO;
    AccountResponseDTO accountResponseDTO;
    List<AccountResponseDTO> accountResponseDTOs;
    AccountResponse accountResponse;
    AccountEditDTO accountEditDTO;
    AccountIdDTO accountIdDTO;

    @BeforeEach
    public void setUp() {
        setUpAccountRequestDTO();
        setUpAccountResponseDTOAndListOfResponses();
        setUpAccountResponse();
        setUpAccountEditDTO();
        setUpAccountIdDTO();
    }

    private void setUpAccountRequestDTO() {
        accountRequestDTO = new AccountRequestDTO();
        accountRequestDTO.setAccountId(1);
        accountRequestDTO.setAccountNumber(123L);
        accountRequestDTO.setAccountType("CA");
        accountRequestDTO.setBalance(1000.5);
    }

    private void setUpAccountResponseDTOAndListOfResponses() {
        accountResponseDTO = new AccountResponseDTO();
        BeanUtils.copyProperties(accountRequestDTO, accountResponseDTO);

        accountResponseDTOs = new ArrayList<>();
        accountResponseDTOs.add(accountResponseDTO);
    }

    private void setUpAccountResponse() {
        Customer customer = new Customer();
        accountResponse = new AccountResponse(123L, 100.5, customer, "CA");
    }

    private void setUpAccountEditDTO() {
        accountEditDTO = new AccountEditDTO();
        BeanUtils.copyProperties(accountRequestDTO, accountEditDTO);
    }

    private void setUpAccountIdDTO() {
        accountIdDTO = new AccountIdDTO();
        accountIdDTO.setAccountId(1);
    }

    @Test
    @DisplayName("Save Account Data")
    public void saveAccountDataTest() {
        //context - ruler
        when(accountService.saveAccountData(accountRequestDTO))
                .thenReturn(accountResponseDTO);

        //event
        ResponseEntity<AccountResponseDTO> result = accountController.saveAccountData(accountRequestDTO);
        assertEquals(accountResponseDTO, result.getBody());
        assertEquals(HttpStatus.CREATED, result.getStatusCode());
    }

    @Test
    @DisplayName("Get All Accounts")
    public void getAccountsDetailsTest() {
        when(accountService.getAccountsDetails()).thenReturn(accountResponseDTOs);

        ResponseEntity<List<AccountResponseDTO>> result = accountController.getAccountsDetails();
        assertEquals(accountResponseDTOs, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Get account by account number")
    public void getAccountByAccountNumberTest() {
        when(accountService.findAccountByAccountNumber(accountRequestDTO.getAccountNumber())).thenReturn(accountResponse);

        ResponseEntity<AccountResponse> result = accountController.getAccountByAccountNumber(accountRequestDTO.getAccountNumber());
        assertEquals(accountResponse, result.getBody());
        assertEquals(HttpStatus.FOUND, result.getStatusCode());
    }

    @Test
    @DisplayName("Update account")
    public void updateAccountTest() {
        when(accountService.updateAccount(accountEditDTO)).thenReturn(accountResponse);

        ResponseEntity<AccountResponse> result = accountController.updateAccount(accountEditDTO);
        assertEquals(accountResponse, result.getBody());
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }

    @Test
    @DisplayName("Delete account")
    public void deleteAccountTest() {
        when(accountService.deleteAccount(accountIdDTO)).thenReturn("Account deleted successfully");

        ResponseEntity<String> result = accountController.deleteAccount(accountIdDTO);
        assertEquals("Account deleted successfully", result.getBody());
        assertEquals(HttpStatus.OK, result.getStatusCode());
    }
}

package com.example.consumerBankApp.dto.accountDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountResponseDTO {
    private Integer accountId;
    private Long accountNumber;

    public AccountResponseDTO() {

    }

    public AccountResponseDTO(Integer accountId, Long accountNumber) {
        this.accountId = accountId;
        this.accountNumber = accountNumber;
    }
}

package com.example.consumerBankApp.dto;

import lombok.Data;

@Data
public class BeneficiaryRequestDTO {
    private Integer beneficiaryCustomerId;
    private Integer benefactorCustomerId;
}

package com.example.consumerBankApp.dto.transactionDTO;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class TransactionEditDTO {
    @NotNull(message = "Transaction ID cannot be empty")
    private Integer transactionId;

    @NotNull(message = "Amount cannot be empty")
    @Min(value = 10, message = "Amount cannot be less than 10")
    private double amount;
}

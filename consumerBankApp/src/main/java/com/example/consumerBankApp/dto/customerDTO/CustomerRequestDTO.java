package com.example.consumerBankApp.dto.customerDTO;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class CustomerRequestDTO {

    private Integer customerId;

    @NotEmpty(message = "name cannot be empty")
    private String customerName;

    @NotEmpty(message = "phoneNo cannot be empty")
    private String phoneNo;

    @NotEmpty(message = "address cannot be empty")
    private String address;
}

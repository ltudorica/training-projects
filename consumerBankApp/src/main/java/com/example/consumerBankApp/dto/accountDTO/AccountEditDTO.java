package com.example.consumerBankApp.dto.accountDTO;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class AccountEditDTO {
    @NotNull(message = "account number cannot be empty")
    private Integer accountId;

    @NotNull(message = "balance cannot be empty")
    @Min(value = 1000, message = "amount cannot be less than 1000")
    private double balance;

    @NotEmpty(message = "Account type cannot be empty")
    private String accountType;
}

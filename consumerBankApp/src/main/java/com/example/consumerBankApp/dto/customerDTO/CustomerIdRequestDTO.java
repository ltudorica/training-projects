package com.example.consumerBankApp.dto.customerDTO;

import com.sun.istack.NotNull;
import lombok.Data;

@Data
public class CustomerIdRequestDTO {

    @NotNull
    private Integer customerId;
}

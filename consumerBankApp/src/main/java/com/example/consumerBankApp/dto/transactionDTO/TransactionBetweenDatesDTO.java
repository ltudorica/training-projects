package com.example.consumerBankApp.dto.transactionDTO;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class TransactionBetweenDatesDTO {
    @NotNull(message = "Customer ID cannot be empty")
    private Integer customerId;

    @NotNull(message = "Start date cannot be empty")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
//    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate startDate;

    @NotNull(message = "End date cannot be empty")
//    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
//    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private LocalDate endDate;
}

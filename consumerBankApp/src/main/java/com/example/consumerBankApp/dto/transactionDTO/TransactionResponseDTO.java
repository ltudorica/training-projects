package com.example.consumerBankApp.dto.transactionDTO;

import com.example.consumerBankApp.entity.Account;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode
public class TransactionResponseDTO {

    private Integer transactionNumber;
    private double amount;
    private String transactionType;
    private Account account;

    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate transactionDate;

    public TransactionResponseDTO() {
    }

    public TransactionResponseDTO(Integer transactionNumber, double amount, String transactionType, Account account, LocalDate transactionDate) {
        this.transactionNumber = transactionNumber;
        this.amount = amount;
        this.transactionType = transactionType;
        this.account = account;
        this.transactionDate = transactionDate;
    }
}

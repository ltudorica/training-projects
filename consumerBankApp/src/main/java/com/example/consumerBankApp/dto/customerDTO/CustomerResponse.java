package com.example.consumerBankApp.dto.customerDTO;

public interface CustomerResponse {

//    @Value("#{target.customerName + ' ' + target.phoneNo}") -> custom
    String getCustomerName();
    String getPhoneNo();
    String getAddress();
}

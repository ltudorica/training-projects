package com.example.consumerBankApp.dto.transactionDTO;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class TransactionRequestDTO {
    @NotNull
    private String transactionNumber;

    @NotNull
    private double amount;

    @NotNull
    private String transactionType;

    @NotNull
    private Integer accountId;

    @NotNull
    private LocalDate transactionDate;
}

package com.example.consumerBankApp.dto.transactionDTO;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class TransferDTO {
//    @NotNull(message = "Transaction number cannot be empty")
//    private Integer transactionNumber;

    @NotNull(message = "Sender account cannot be empty")
    private Long senderAccountNumber;

    @NotNull(message = "Receiver account cannot be empty")
    private Long receiverAccountNumber;

    @NotNull(message = "Amount cannot be empty")
    @Min(value = 10, message = "Amount cannot be less than 10")
    private double amount;
}

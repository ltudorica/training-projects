package com.example.consumerBankApp.dto.accountDTO;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AccountIdDTO {

    @NotNull(message = "Account number cannot be empty")
    private Integer accountId;
}

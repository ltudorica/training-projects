package com.example.consumerBankApp.dto.customerDTO;

import com.example.consumerBankApp.entity.Account;
import com.sun.istack.NotNull;
import lombok.Data;

import java.util.List;

@Data
public class CustomerResponseDTO {
    @NotNull
    private Integer customerId;
    @NotNull
    private String customerName;
    @NotNull
    private String phoneNo;
    @NotNull
    private String address;
    private List<Account> accounts;
}

package com.example.consumerBankApp.dto.accountDTO;

import com.example.consumerBankApp.entity.Customer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountResponse {
    private Long accountNumber;
    private double balance;
    private Customer customer;
    private String accountType;

    public AccountResponse(Long accountNumber, double balance, Customer customer, String accountType) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customer = customer;
        this.accountType = accountType;
    }

}

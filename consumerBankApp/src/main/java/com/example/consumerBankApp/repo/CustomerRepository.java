package com.example.consumerBankApp.repo;

import com.example.consumerBankApp.dto.customerDTO.CustomerResponse;
import com.example.consumerBankApp.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

//    INTERFACE PROJECTION
    List<CustomerResponse> findByCustomerNameLike(String customerName);

    @Query(value = "SELECT c FROM Customer c WHERE c.phoneNo = :phoneNo AND c.address = :address", nativeQuery = true)
    Customer findCustomerData(String phoneNo, String address);

//    @Query(value = "SELECT c FROM Customer c WHERE c.phoneNo = :phoneNo", nativeQuery = true)
    Customer findCustomerByPhoneNo(String phoneNo);
}

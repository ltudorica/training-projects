package com.example.consumerBankApp.repo;

import com.example.consumerBankApp.dto.accountDTO.AccountResponse;
import com.example.consumerBankApp.dto.accountDTO.AccountResponseDTO;
import com.example.consumerBankApp.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    //    DTO PROJECTION
    @Query(value = "select new com.example.consumerBankApp.dto.accountDTO.AccountResponseDTO(a.accountId, a.accountNumber) from Account a")
    List<AccountResponseDTO> findAccounts();

    //    CLASS PROJECTION
    AccountResponse findDTOByAccountNumber(Long accountNumber);
    Account findByAccountNumber(Long accountNumber);
    List<Account> findByCustomer_CustomerId(Integer customerId);
}

package com.example.consumerBankApp.repo;

import com.example.consumerBankApp.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    @Query(value = "SELECT * FROM Transaction t WHERE MONTH(t.transaction_date) = :month", nativeQuery = true)
    List<Transaction> getTransactionsByMonth(Integer month);

    List<Transaction> findTransactionsByTransactionDateBetween(@Param("startDate") LocalDate startDate,
                                                               @Param("endDate") LocalDate endDate);

    List<Transaction> findByAccount_AccountId(Integer account_id);

    @Query(value = "SELECT * FROM Transaction t WHERE t.transaction_number = :transactionNumber",  nativeQuery = true)
    List<Transaction> findByTransactionNumber(Integer transactionNumber);

}

package com.example.consumerBankApp.service;

import com.example.consumerBankApp.dto.customerDTO.CustomerIdRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponse;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponseDTO;

import java.util.List;

public interface CustomerService {
    String saveCustomerData(CustomerRequestDTO customerRequestDTO);
    List<CustomerResponseDTO> getCustomerDetails();
    CustomerResponseDTO getCustomerDetails(Integer customerId);
    List<CustomerResponse> getCustomerDetails(String name);
    CustomerResponseDTO updateCustomer(CustomerRequestDTO customerRequestDTO);
    String deleteCustomer(CustomerIdRequestDTO customerIdRequestDTO);

}

package com.example.consumerBankApp.service;

import com.example.consumerBankApp.dto.accountDTO.*;

import java.util.List;

public interface AccountService {
    AccountResponseDTO saveAccountData(AccountRequestDTO accountRequestDTO);

    List<AccountResponseDTO> getAccountsDetails();

    AccountResponse updateAccount(AccountEditDTO accountEditDTO);

    AccountResponse findAccountByAccountNumber(Long accountNumber);

    String deleteAccount(AccountIdDTO accountIdDTO);
}

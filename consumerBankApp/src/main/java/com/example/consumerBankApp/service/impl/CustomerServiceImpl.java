package com.example.consumerBankApp.service.impl;

import com.example.consumerBankApp.dto.customerDTO.CustomerIdRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponse;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponseDTO;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.exception.CustomerAlreadyExistException;
import com.example.consumerBankApp.exception.CustomerNotFoundException;
import com.example.consumerBankApp.repo.CustomerRepository;
import com.example.consumerBankApp.service.CustomerService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public String saveCustomerData(CustomerRequestDTO customerRequestDTO) {
        if(customerRepository.findCustomerByPhoneNo(customerRequestDTO.getPhoneNo()) != null) {
            throw new CustomerAlreadyExistException("This phone number is used by another customer");
        }
        Customer customer = new Customer();
        BeanUtils.copyProperties(customerRequestDTO, customer);
        customerRepository.save(customer);
        return "Data saved successfully for customer: " + customerRequestDTO.getCustomerName();
    }

    @Override
    public List<CustomerResponseDTO> getCustomerDetails() {
        List<CustomerResponseDTO> customerResponseDTOList = new ArrayList<>();
        List<Customer> customerList = customerRepository.findAll();

        customerList.forEach(Customer -> {
            CustomerResponseDTO responseDTO = new CustomerResponseDTO();
            BeanUtils.copyProperties(Customer, responseDTO);
            customerResponseDTOList.add(responseDTO);
        });

        return customerResponseDTOList;
    }

    @Override
    public CustomerResponseDTO getCustomerDetails(Integer customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
        if(optionalCustomer.isEmpty()) {
            throw new CustomerNotFoundException("There is no customer for id " + customerId);
        }
        CustomerResponseDTO responseDTO = new CustomerResponseDTO();
        Customer customer = optionalCustomer.get();
        BeanUtils.copyProperties(customer, responseDTO);
        return responseDTO;
    }

    @Override
    public List<CustomerResponse> getCustomerDetails(String name) {
        List<CustomerResponse> customers = customerRepository.findByCustomerNameLike(name);
        if (customers.isEmpty()) {
            throw new CustomerNotFoundException("No customers found under " + name + " name");
        }
        return customers;
    }

    @Override
    public CustomerResponseDTO updateCustomer(CustomerRequestDTO customerRequestDTO) {
        if(customerRequestDTO.getCustomerId() == null) {
            throw new CustomerNotFoundException("There is no id to search, please try again!");
        }
        Optional<Customer> optionalCustomer = customerRepository.findById(customerRequestDTO.getCustomerId());
        if (optionalCustomer.isEmpty()) {
            throw new CustomerNotFoundException("There is no customer for id " + customerRequestDTO.getCustomerId());
        }
        if(customerRepository.findCustomerByPhoneNo(customerRequestDTO.getPhoneNo()) != null) {
            throw new CustomerAlreadyExistException("This phone number is used by another customer");
        }
        Customer customer = optionalCustomer.get();
        customer.setCustomerName(customerRequestDTO.getCustomerName());
        customer.setAddress(customerRequestDTO.getAddress());
        customer.setPhoneNo(customerRequestDTO.getPhoneNo());
        customerRepository.save(customer);
        CustomerResponseDTO responseDTO = new CustomerResponseDTO();
        BeanUtils.copyProperties(customer, responseDTO);
        return responseDTO;
    }

    @Override
    public String deleteCustomer(CustomerIdRequestDTO customerIdRequestDTO) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerIdRequestDTO.getCustomerId());
        if(optionalCustomer.isEmpty()) {
            throw new CustomerNotFoundException("There is no customer for id " + customerIdRequestDTO.getCustomerId());
        }
        customerRepository.deleteById(customerIdRequestDTO.getCustomerId());
        return "Customer deleted successfully";
    }
}

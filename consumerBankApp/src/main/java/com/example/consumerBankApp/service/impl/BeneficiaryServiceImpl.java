package com.example.consumerBankApp.service.impl;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.BeneficiaryResponseDto;
import com.example.consumerBankApp.entity.Beneficiary;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.exception.CustomerNotFoundException;
import com.example.consumerBankApp.repo.BeneficiaryRepository;
import com.example.consumerBankApp.repo.CustomerRepository;
import com.example.consumerBankApp.service.BeneficiaryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BeneficiaryServiceImpl implements BeneficiaryService {

    @Autowired
    BeneficiaryRepository beneficiaryRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public BeneficiaryResponseDto saveBeneficiary(BeneficiaryRequestDTO beneficiaryRequestDTO) {
        Optional<Customer> beneficiaryCustomer = customerRepository
                .findById(beneficiaryRequestDTO.getBeneficiaryCustomerId());
        if (beneficiaryCustomer.isEmpty()) {
            throw new CustomerNotFoundException("Customer doesn't exist for the Id: " + beneficiaryRequestDTO.getBeneficiaryCustomerId());
        }

        Optional<Customer> benefactorCustomer = customerRepository.findById(beneficiaryRequestDTO.getBenefactorCustomerId());
        if (benefactorCustomer.isEmpty()) {
            throw new CustomerNotFoundException("Customer doesn't exist for the Id: " +
                    beneficiaryRequestDTO.getBenefactorCustomerId());
        }

        Beneficiary beneficiary = new Beneficiary();
        BeanUtils.copyProperties(beneficiaryRequestDTO, beneficiary);

        beneficiaryRepository.save(beneficiary);

        BeneficiaryResponseDto beneficiaryResponseDto = new BeneficiaryResponseDto();
        BeanUtils.copyProperties(beneficiary, beneficiaryResponseDto);

        return beneficiaryResponseDto;
    }
}

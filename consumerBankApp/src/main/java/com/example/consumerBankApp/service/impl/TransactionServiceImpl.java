package com.example.consumerBankApp.service.impl;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionBetweenDatesDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionEditDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionResponseDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransferDTO;
import com.example.consumerBankApp.entity.Account;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.entity.Transaction;
import com.example.consumerBankApp.exception.*;
import com.example.consumerBankApp.repo.AccountRepository;
import com.example.consumerBankApp.repo.CustomerRepository;
import com.example.consumerBankApp.repo.TransactionRepository;
import com.example.consumerBankApp.service.TransactionService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public String transferFunds(TransferDTO transferDTO, BeneficiaryRequestDTO beneficiaryRequestDTO) {
        Optional<Customer> optionalBenefactor = customerRepository.findById(beneficiaryRequestDTO.getBenefactorCustomerId());
        Optional<Customer> optionalBeneficiary = customerRepository.findById(beneficiaryRequestDTO.getBeneficiaryCustomerId());
        if (optionalBenefactor.isEmpty() || optionalBeneficiary.isEmpty()) {
            throw new CustomerNotFoundException(
                    "One of the customers / or both cannot be found. Please check and try again.");
        }

        if(accountRepository.findByAccountNumber(transferDTO.getSenderAccountNumber()) == null ||
                accountRepository.findByAccountNumber(transferDTO.getReceiverAccountNumber()) == null) {
            throw new AccountNotExistException("One of the accounts / or both cannot be found. Please check and try again.");
        }

        Customer sender = optionalBenefactor.get();
        Customer receiver = optionalBenefactor.get();
        Integer transactionNumber = ThreadLocalRandom.current().nextInt();
        for (Account account : sender.getAccounts()) {
            if (transferDTO.getSenderAccountNumber().equals(account.getAccountNumber())) {
                if(account.getBalance() < transferDTO.getAmount()) {
                    throw new InsufficientFundsException("Insufficient funds for making this transaction. Balance is " + account.getBalance());
                }
                saveDebitTransaction(transactionNumber, transferDTO, account);
                account.setBalance(account.getBalance() - transferDTO.getAmount());
                accountRepository.save(account);
            }
        }

        for (Account account : receiver.getAccounts()) {
            if (transferDTO.getReceiverAccountNumber().equals(account.getAccountNumber())) {
                saveCreditTransaction(transactionNumber, transferDTO, account);
                account.setBalance(account.getBalance() + transferDTO.getAmount());
                accountRepository.save(account);
            }
        }
        customerRepository.save(sender);
        customerRepository.save(receiver);

        return "Transaction completed";
    }

    void saveDebitTransaction(Integer transactionNumber, TransferDTO transferDTO, Account account) {
        Transaction transaction = new Transaction();
        transaction.setTransactionNumber(transactionNumber);
        transaction.setAmount(-transferDTO.getAmount());
        transaction.setAccount(account);
        transaction.setTransactionDate(LocalDate.now());
        transaction.setTransactionType("Debit");
        transactionRepository.save(transaction);
    }

    void saveCreditTransaction(Integer transactionNumber, TransferDTO transferDTO, Account account) {
        Transaction transaction = new Transaction();
        transaction.setTransactionNumber(transactionNumber);
        transaction.setAmount(transferDTO.getAmount());
        transaction.setAccount(account);
        transaction.setTransactionDate(LocalDate.now());
        transaction.setTransactionType("Credit");
        transactionRepository.save(transaction);
    }

    @Override
    public List<TransactionResponseDTO> getTransactions() {
        List<TransactionResponseDTO> result = new ArrayList<>();
        List<Transaction> transactions = transactionRepository.findAll();
        transactions.forEach(Transaction -> result.add(convertTransactionToDTO(Transaction)));
        return result;
    }

    @Override
    public TransactionResponseDTO getTransaction(Integer transactionId) {
        Optional<Transaction> optional = transactionRepository.findById(transactionId);
        if(optional.isEmpty()) {
            throw new TransactionNotFoundException("Transaction cannot be found");
        }
        Transaction transaction = optional.get();
        TransactionResponseDTO responseDTO = new TransactionResponseDTO();
        BeanUtils.copyProperties(transaction, responseDTO);
        return responseDTO;
    }

    @Override
    public TransactionResponseDTO updateTransactionData(TransactionEditDTO transactionEditDTO) {
        if(transactionRepository.findById(transactionEditDTO.getTransactionId()).isEmpty()) {
            throw new TransactionNotFoundException("Transaction cannot be found");
        }
        Transaction transaction = transactionRepository.findById(transactionEditDTO.getTransactionId()).get();
        transaction.setAmount(transactionEditDTO.getAmount());
        transactionRepository.save(transaction);
        TransactionResponseDTO responseDTO = new TransactionResponseDTO();
        BeanUtils.copyProperties(transaction, responseDTO);
        return responseDTO;
    }

    @Override
    public String delete(Integer transactionId) {
        if(transactionRepository.findById(transactionId).isEmpty()) {
            throw new TransactionNotFoundException("Transaction cannot be found!");
        }
        transactionRepository.deleteById(transactionId);
        return "Transaction was deleted";
    }

    @Override
    public List<TransactionResponseDTO> getTransactionsForAccountNumber(Long accountNumber) {
        if(accountRepository.findByAccountNumber(accountNumber) == null) {
            throw new AccountNotExistException("The account cannot be found");
        }
        Account account = accountRepository.findByAccountNumber(accountNumber);
        List<TransactionResponseDTO> result = new ArrayList<>();
        List<Transaction> transactions = transactionRepository.findByAccount_AccountId(account.getAccountId());
        transactions.forEach(Transaction -> result.add(convertTransactionToDTO(Transaction)));
        return result;
    }

    private TransactionResponseDTO convertTransactionToDTO(Transaction transaction) {
        TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
        BeanUtils.copyProperties(transaction, transactionResponseDTO);
        return transactionResponseDTO;
    }

    //todo TEST
    @Override
    public List<TransactionResponseDTO> getTransactionsForCustomer(Integer customerId) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
        if(optionalCustomer.isEmpty()) {
            throw new CustomerNotFoundException("The customer cannot be found!");
        }
        List<Transaction> transactions = new ArrayList<>();
        List<Account> accounts = accountRepository.findByCustomer_CustomerId(customerId);
        accounts.forEach(Account -> transactions.addAll(transactionRepository.findByAccount_AccountId(Account.getAccountId())));
        List<TransactionResponseDTO> result = new ArrayList<>();
        transactions.forEach(Transaction -> result.add(convertTransactionToDTO(Transaction)));

        return result;
    }

    @Override
    public List<TransactionResponseDTO> getTransactionsByMonth(Integer customerId, Integer transactionMonth) {
        Optional<Customer> optionalCustomer = customerRepository.findById(customerId);
        if(optionalCustomer.isEmpty()) {
            throw new CustomerNotFoundException("The customer cannot be found!");
        }
        List<Transaction> transactions = transactionRepository.getTransactionsByMonth(transactionMonth);
        return getTransactionsDTOForCustomer(customerId, transactions);
    }

    @Override
    public List<TransactionResponseDTO> getTransactionsBetweenDates(TransactionBetweenDatesDTO transactionBetweenDatesDTO) {
        Optional<Customer> optionalCustomer = customerRepository.findById(transactionBetweenDatesDTO.getCustomerId());
        if(optionalCustomer.isEmpty()) {
            throw new CustomerNotFoundException("The customer cannot be found!");
        }

        List<Transaction> transactions = transactionRepository.findTransactionsByTransactionDateBetween
                (transactionBetweenDatesDTO.getStartDate(), transactionBetweenDatesDTO.getEndDate());
        return getTransactionsDTOForCustomer(transactionBetweenDatesDTO.getCustomerId(), transactions);
    }

     public List<TransactionResponseDTO> getTransactionsDTOForCustomer(Integer customerId, List<Transaction> transactions) {
        List<TransactionResponseDTO> result = new ArrayList<>();
        transactions.forEach(Transaction -> {
            if(Transaction.getAccount().getCustomer().getCustomerId().equals(customerId)) {
                TransactionResponseDTO dto = convertTransactionToDTO(Transaction);
                result.add(dto);
            }
        });
        return result;
    }

    //    @Override
//    public String transferFunds(TransferDTO transferDTO) {
//        if(accountRepository.findByAccountNumber(transferDTO.getSenderAccountNumber()) == null ||
//                accountRepository.findByAccountNumber(transferDTO.getReceiverAccountNumber()) == null) {
//            throw new AccountNotExistException("One of the accounts / or both cannot be found. Please check and try again.");
//        }
//        Account senderAccount = accountRepository.findByAccountNumber(transferDTO.getSenderAccountNumber());
//        Account receiverAccount = accountRepository.findByAccountNumber(transferDTO.getReceiverAccountNumber());
//        if(senderAccount.getBalance() < transferDTO.getAmount()) {
//            throw new InsufficientFundsException("Insufficient funds for making this transaction. Balance is " +
//                    senderAccount.getBalance());
//        }
//
//        Integer transactionNumber = ThreadLocalRandom.current().nextInt();
//
//        saveDebitTransaction(transactionNumber, transferDTO, senderAccount);
//        senderAccount.setBalance(senderAccount.getBalance() - transferDTO.getAmount());
//        accountRepository.save(senderAccount);
//
//        saveCreditTransaction(transactionNumber, transferDTO, receiverAccount);
//        receiverAccount.setBalance(receiverAccount.getBalance() + transferDTO.getAmount());
//        accountRepository.save(receiverAccount);
//        return "Transaction completed";
//    }
}

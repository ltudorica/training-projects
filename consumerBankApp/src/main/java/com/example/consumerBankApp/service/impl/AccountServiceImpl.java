package com.example.consumerBankApp.service.impl;

import com.example.consumerBankApp.dto.accountDTO.*;
import com.example.consumerBankApp.entity.Account;
import com.example.consumerBankApp.entity.Customer;
import com.example.consumerBankApp.exception.AccountAlreadyExistException;
import com.example.consumerBankApp.exception.AccountNotExistException;
import com.example.consumerBankApp.exception.CustomerNotFoundException;
import com.example.consumerBankApp.repo.AccountRepository;
import com.example.consumerBankApp.repo.CustomerRepository;
import com.example.consumerBankApp.service.AccountService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public AccountResponseDTO saveAccountData(AccountRequestDTO accountRequestDTO) {
        Optional<Customer> optionalCustomer = customerRepository.findById(accountRequestDTO.getCustomer().getCustomerId());
        if(optionalCustomer.isEmpty()) {
            throw new CustomerNotFoundException("Customer doesn't exist for the id "
                    + accountRequestDTO.getCustomer().getCustomerId() +
                    ", please assign the account to an existing customer!");
        }
        if(accountRepository.findByAccountNumber(accountRequestDTO.getAccountNumber()) != null) {
            throw new AccountAlreadyExistException("Cannot assign this account, it is already in use!");
        }

        Account account = new Account();
        BeanUtils.copyProperties(accountRequestDTO, account);
        accountRepository.save(account);
        AccountResponseDTO accountResponseDTO = new AccountResponseDTO();
        BeanUtils.copyProperties(account, accountResponseDTO);
        return accountResponseDTO;
    }

    @Override
    public List<AccountResponseDTO> getAccountsDetails() {
        List<AccountResponseDTO> accounts = accountRepository.findAccounts();
        if(accounts.isEmpty()) {
            throw new AccountNotExistException("No accounts found");
        }
        return accounts;
    }

    @Override
    public AccountResponse findAccountByAccountNumber(Long accountNumber) {
        if(accountRepository.findByAccountNumber(accountNumber) == null) {
            throw new AccountNotExistException("There is no account found for account number " + accountNumber);
        }
        return accountRepository.findDTOByAccountNumber(accountNumber);
    }

    @Override
    public AccountResponse updateAccount(AccountEditDTO accountEditDTO) {
        Optional<Account> optionalAccount = accountRepository.findById(accountEditDTO.getAccountId());
        if(optionalAccount.isEmpty()) {
            throw new AccountNotExistException("There is no account found for id " + accountEditDTO.getAccountId());
        }
        Account account = optionalAccount.get();
        account.setAccountType(accountEditDTO.getAccountType());
        account.setBalance(accountEditDTO.getBalance());
        accountRepository.save(account);
        return new AccountResponse(account.getAccountNumber(), account.getBalance(), account.getCustomer(), account.getAccountType());
    }

    @Override
    public String deleteAccount(AccountIdDTO accountIdDTO) {
        Optional<Account> optionalAccount = accountRepository.findById(accountIdDTO.getAccountId());
        if(optionalAccount.isEmpty()) {
            throw new AccountNotExistException("There is no account found for id " + accountIdDTO.getAccountId());
        }
        accountRepository.delete(optionalAccount.get());
        return "Account deleted successfully";
    }

}

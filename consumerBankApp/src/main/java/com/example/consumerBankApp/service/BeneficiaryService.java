package com.example.consumerBankApp.service;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.BeneficiaryResponseDto;

public interface BeneficiaryService {
    BeneficiaryResponseDto saveBeneficiary(BeneficiaryRequestDTO beneficiaryRequestDTO);
}

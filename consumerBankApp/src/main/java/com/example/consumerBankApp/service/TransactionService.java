package com.example.consumerBankApp.service;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.transactionDTO.*;

import java.util.List;

public interface TransactionService {

    String transferFunds(TransferDTO transferDTO, BeneficiaryRequestDTO beneficiaryRequestDTO);

    List<TransactionResponseDTO> getTransactions();

    TransactionResponseDTO getTransaction(Integer transactionId);

    List<TransactionResponseDTO> getTransactionsForCustomer(Integer customerId);

    TransactionResponseDTO updateTransactionData( TransactionEditDTO transactionEditDTO);

    String delete(Integer transactionId);

    List<TransactionResponseDTO> getTransactionsForAccountNumber(Long accountNumber);

    List<TransactionResponseDTO> getTransactionsByMonth(Integer customerId, Integer transactionMonth);

    List<TransactionResponseDTO> getTransactionsBetweenDates(TransactionBetweenDatesDTO transactionBetweenDatesDTO);

}

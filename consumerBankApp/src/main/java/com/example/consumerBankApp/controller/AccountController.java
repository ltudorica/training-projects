package com.example.consumerBankApp.controller;

import com.example.consumerBankApp.dto.accountDTO.*;
import com.example.consumerBankApp.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    AccountService accountService;

    @PostMapping
    public ResponseEntity<AccountResponseDTO> saveAccountData(@Valid @RequestBody AccountRequestDTO accountRequestDTO) {
        log.info("ACCOUNT CONTROLLER INFO ******** Saving the account data ********");
        AccountResponseDTO accountResponseDTO = accountService.saveAccountData(accountRequestDTO);
        return new ResponseEntity<>(accountResponseDTO, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<AccountResponseDTO>> getAccountsDetails() {
        log.info("ACCOUNT CONTROLLER INFO ******** Get all accounts ********");
        return new ResponseEntity<>(accountService.getAccountsDetails(), HttpStatus.FOUND);
    }

    @PostMapping("/{accountNumber}")
    public ResponseEntity<AccountResponse> getAccountByAccountNumber(
            @NotNull(message = "Account number cannot be empty") @PathVariable Long accountNumber) {
        log.info("ACCOUNT CONTROLLER INFO ******** Get account by acc. number ********");
        AccountResponse accountResponse = accountService.findAccountByAccountNumber(accountNumber);
        return new ResponseEntity<>(accountResponse, HttpStatus.FOUND);
    }

    @PutMapping
    public ResponseEntity<AccountResponse> updateAccount(@Valid @RequestBody AccountEditDTO accountEditDTO) {
        log.info("ACCOUNT CONTROLLER INFO ******** Update account ********");
        return new ResponseEntity<>(accountService.updateAccount(accountEditDTO), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<String> deleteAccount(@Valid @RequestBody AccountIdDTO accountIdDTO) {
        log.info("ACCOUNT CONTROLLER INFO ******** Delete account ********");
        return new ResponseEntity<>(accountService.deleteAccount(accountIdDTO), HttpStatus.OK);
    }

}

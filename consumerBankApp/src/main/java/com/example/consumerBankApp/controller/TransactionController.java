package com.example.consumerBankApp.controller;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionBetweenDatesDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionEditDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransactionResponseDTO;
import com.example.consumerBankApp.dto.transactionDTO.TransferDTO;
import com.example.consumerBankApp.service.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@RestController
@Slf4j
@Validated
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @PostMapping("/transactions")
    public ResponseEntity<String> transferFunds(@Valid @RequestBody TransferDTO transferDTO, BeneficiaryRequestDTO beneficiaryRequestDTO) {
        log.info("TRANSACTION CONTROLLER INFO ******** Making a transaction ********");

        return new ResponseEntity<>(transactionService.transferFunds(transferDTO, beneficiaryRequestDTO), HttpStatus.ACCEPTED);
    }

    @GetMapping("/transactions")
    public ResponseEntity<List<TransactionResponseDTO>> getTransactions() {
        log.info("TRANSACTION CONTROLLER INFO ******** Get all transactions ********");

        return new ResponseEntity<>(transactionService.getTransactions(), HttpStatus.FOUND);
    }

    @GetMapping("/transactions/{transactionId}")
    public ResponseEntity<TransactionResponseDTO> getTransaction(@NotEmpty(message = "Transaction id cannot be empty") @PathVariable Integer transactionId) {
        log.info("TRANSACTION CONTROLLER INFO ******** Get transaction by id ********");

        return new ResponseEntity<>(transactionService.getTransaction(transactionId), HttpStatus.FOUND);
    }

    @GetMapping(value = "/transactions", params = {"account"})
    public ResponseEntity<List<TransactionResponseDTO>> getTransactionsForAccountNumber
            (@NotNull(message = "Account number cannot be empty") @RequestParam Long account) {
        log.info("TRANSACTION CONTROLLER INFO ******** Get transaction by account number ********");

        return new ResponseEntity<>(transactionService.getTransactionsForAccountNumber(account), HttpStatus.FOUND);
    }

    @PutMapping("/transactions")
    public ResponseEntity<TransactionResponseDTO> updateTransactionData(@Valid @RequestBody TransactionEditDTO transactionEditDTO) {
        log.info("TRANSACTION CONTROLLER INFO ******** Update transaction ********");

        TransactionResponseDTO responseDTO = transactionService.updateTransactionData(transactionEditDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/transactions/{transactionId}")
    public ResponseEntity<String> deleteTransaction(@NotNull(message = "Transaction Id cannot be empty") @PathVariable Integer transactionId) {
        log.info("TRANSACTION CONTROLLER INFO ******** Delete transaction ********");

        return new ResponseEntity<>(transactionService.delete(transactionId), HttpStatus.ACCEPTED);
    }

    @GetMapping("/customers/{customerId}/transactions")
    public ResponseEntity<List<TransactionResponseDTO>> getTransactionsForCustomer
            (@NotNull(message = "Customer id cannot be null") @PathVariable Integer customerId) {
        log.info("TRANSACTION CONTROLLER INFO ******** Get transactions by customer id ********");

        return new ResponseEntity<>(transactionService.getTransactionsForCustomer(customerId), HttpStatus.FOUND);
    }

    @GetMapping(value = "/customers/{customerId}/transactions", params = {"month"})
    public ResponseEntity<List<TransactionResponseDTO>> getTransactionsByMonth
            (@NotNull(message = "Customer id cannot be null") @PathVariable Integer customerId,
             @NotNull(message = "Month cannot be null") @RequestParam Integer month) {
        log.info("TRANSACTION CONTROLLER INFO ******** Get transactions by month ********");

        return new ResponseEntity<>(transactionService.getTransactionsByMonth(customerId, month), HttpStatus.FOUND);
    }

    @GetMapping(value = "/transactions/by_period")
    public ResponseEntity<List<TransactionResponseDTO>> getTransactionsBetweenDates
            (@RequestBody TransactionBetweenDatesDTO transactionBetweenDatesDTO) {
        log.info("TRANSACTION CONTROLLER INFO ******** Get transactions between dates ********");

        return new ResponseEntity<>(transactionService.getTransactionsBetweenDates(transactionBetweenDatesDTO), HttpStatus.ACCEPTED);
    }
}

package com.example.consumerBankApp.controller;

import com.example.consumerBankApp.dto.customerDTO.CustomerIdRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerRequestDTO;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponse;
import com.example.consumerBankApp.dto.customerDTO.CustomerResponseDTO;
import com.example.consumerBankApp.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@RestController
@Validated
@Slf4j
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @PostMapping("/customers")
    public ResponseEntity<String> saveCustomerData(@Valid @RequestBody CustomerRequestDTO customerRequestDTO) {
        log.info("CUSTOMER CONTROLLER INFO ******** Saving the customer data ********");
        return new ResponseEntity<>(customerService.saveCustomerData(customerRequestDTO), HttpStatus.ACCEPTED);
    }

    @GetMapping("/customers")
    public ResponseEntity<List<CustomerResponseDTO>> getCustomersDetails() {
        log.info("CUSTOMER CONTROLLER INFO ******** Get all customer data ********");
        return new ResponseEntity<>(customerService.getCustomerDetails(), HttpStatus.FOUND);
    }

    @GetMapping("/customers/{customerId}")
    public ResponseEntity<CustomerResponseDTO> getCustomerDetails(@NotEmpty(message = "Customer id cannot be empty") @PathVariable Integer customerId) {
        log.info("CUSTOMER CONTROLLER INFO ******** Get customer by id ********");
        return new ResponseEntity<>(customerService.getCustomerDetails(customerId), HttpStatus.FOUND);
    }

    @GetMapping(params = {"name"}, value = "/customers")
    public ResponseEntity<List<CustomerResponse>> getCustomerDetails(@NotEmpty(message = "name cannot be empty") @RequestParam String name) {
        log.info("CUSTOMER CONTROLLER INFO ******** Get customer by name ********");
        return new ResponseEntity<>(customerService.getCustomerDetails(name), HttpStatus.FOUND);
    }

    @PutMapping("/customers")
    public ResponseEntity<CustomerResponseDTO> updateCustomer(@Valid @RequestBody CustomerRequestDTO customerRequestDTO) {
        log.info("CUSTOMER CONTROLLER INFO ******** Update customer ********");
        return new ResponseEntity<>(customerService.updateCustomer(customerRequestDTO), HttpStatus.OK);
    }

    @DeleteMapping("/customers")
    public ResponseEntity<String> deleteCustomer(@Valid @RequestBody CustomerIdRequestDTO customerIdRequestDTO) {
        log.info("CUSTOMER CONTROLLER INFO ******** Delete customer ********");
        return new ResponseEntity<>( customerService.deleteCustomer(customerIdRequestDTO), HttpStatus.OK);
    }
}

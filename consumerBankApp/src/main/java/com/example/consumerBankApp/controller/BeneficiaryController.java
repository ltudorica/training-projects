package com.example.consumerBankApp.controller;

import com.example.consumerBankApp.dto.BeneficiaryRequestDTO;
import com.example.consumerBankApp.dto.BeneficiaryResponseDto;
import com.example.consumerBankApp.service.BeneficiaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
public class BeneficiaryController {

    @Autowired
    BeneficiaryService beneficiaryService;

    @PostMapping("/beneficiaries")
    public ResponseEntity<BeneficiaryResponseDto> saveBeneficiary(
            @RequestBody BeneficiaryRequestDTO beneficiaryRequestDTO) {
        BeneficiaryResponseDto responseDTO = beneficiaryService.saveBeneficiary(beneficiaryRequestDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

}

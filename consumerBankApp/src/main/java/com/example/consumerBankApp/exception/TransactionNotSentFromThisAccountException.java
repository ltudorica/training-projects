package com.example.consumerBankApp.exception;

public class TransactionNotSentFromThisAccountException extends RuntimeException {
    private static final long serialVersionUID = 1L;
    public TransactionNotSentFromThisAccountException(String s) {
        super(s);
    }
}

package com.example.consumerBankApp.exception;

public class AccountAlreadyExistException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AccountAlreadyExistException(String s) {
        super(s);
    }
}
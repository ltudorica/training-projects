package com.example.consumerBankApp.exception;

public class AccountNotExistException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public AccountNotExistException(String s) {
        super(s);
    }
}
package com.example.consumerBankApp.exception;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ErrorResponse {
    private String message;
    private int statusCode;
    private LocalDateTime dateTime;
}

package com.example.consumerBankApp.exception;

public class TransactionNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TransactionNotFoundException(String s) {
        super(s);
    }
}

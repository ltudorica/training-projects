package com.example.consumerBankApp.exception;

public class InsufficientFundsException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public InsufficientFundsException(String s) {
        super(s);
    }
}

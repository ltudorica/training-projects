package com.example.consumerBankApp.exception;

public class TransactionWithNumberAlreadyExist extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public TransactionWithNumberAlreadyExist(String s) {
        super(s);
    }
}

package com.example.consumerBankApp.exception;

public class CustomerAlreadyExistException  extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public CustomerAlreadyExistException(String s) {
        super(s);
    }
}